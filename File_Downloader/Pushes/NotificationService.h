//
//  NotificationService.h
//  Pushes
//
//  Created by Vlad Yalovenko on 21/11/2016.
//  Copyright © 2016 Vlad Yalovenko. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
