//
//  DownloadManager.m
//  File Downloader
//
//  Created by Vlad Yalovenko on 14/11/2016.
//  Copyright © 2016 Vlad Yalovenko. All rights reserved.
//
#import "DownloadManager.h"
@interface DownloadManager ()
@property(nonatomic, retain) NSURLSession *session;
@property(nonatomic, retain) NSMutableArray *arrayFileDownloadData;
@property(nonatomic, retain) NSURL *docDirectoryURL;
@property(nonatomic) DownloadMode downloadMode;
@property(nonatomic) dispatch_semaphore_t semaphore;
@property(nonatomic) dispatch_queue_t aQueue;
@property(nonatomic) dispatch_group_t group;
@property(nonatomic, copy) complitionBlock block;
@end
@implementation DownloadManager

- (instancetype)init
{
    self = [super init];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPMaximumConnectionsPerHost = 5;
    
    self.session = [NSURLSession sessionWithConfiguration:sessionConfiguration
                                                 delegate:self
                                            delegateQueue:nil];
    NSArray *URLs = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    self.docDirectoryURL = [URLs objectAtIndex:0];
    return self;
}

- (void)dealloc
{
    [_docDirectoryURL release];
    [_session release];
    [_arrayFileDownloadData release];
    [_block release];
    [super dealloc];
}

#pragma DownloadManagerProtocol
- (void)downloadItemsForMode:(DownloadMode)mode WithComplition:(complitionBlock)updateBlock
{
    _aQueue = dispatch_queue_create("newQueue", DISPATCH_QUEUE_CONCURRENT);
    [self setDownloadMode:mode];
    [self setBlock:updateBlock];
    [self initializeFileDownloadDataArray];
    
    if ([self downloadMode] == dispatch_semaphore)
        [self downloadItemsForSemaphoreMode];
    else if ([self downloadMode]  == dispatch_group)
        [self downloadItemsForGroupMode];
    else if ([self downloadMode]  == dispatch_main_queue)
        [self downloadItemsForMainMode];
    else if ([self downloadMode]  == dispatch_main_queue_sync)
        [self downloadItemsForMainModeSync];
    else if ([self downloadMode]  == dispatch_global_queue)
        [self downloadItemsForGlobalQueue];
    else if ([self downloadMode]  == dispatch_different_primary_queue)
        [self downloadItemsForDiffentPrimaryMode];
}

- (NSArray *)getDownloadingItems
{
    return [self arrayFileDownloadData];
}

#pragma NSURLSession delegate
- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    NSError *error;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *destinationFilename = downloadTask.originalRequest.URL.lastPathComponent;
    NSURL *destinationURL = [self.docDirectoryURL URLByAppendingPathComponent:destinationFilename];
    
    if ([fileManager fileExistsAtPath:[destinationURL path]])
    {
        [fileManager removeItemAtURL:destinationURL error:nil];
    }

    BOOL success = [fileManager copyItemAtURL:location
                                        toURL:destinationURL
                                        error:&error];
    
    if (success)
    {
        int index = [self getFileDownloadInfoIndexWithTaskIdentifier:downloadTask.taskIdentifier];
        Item *item = [[self arrayFileDownloadData] objectAtIndex:index];
        item.downloadComplete = YES;
        item.identifier = -1;
        [self block]([self arrayFileDownloadData]);
        [self notifyForDownload:item];
    }
    else
    {
        NSLog(@"Unable to copy temp file. Error: %@", [error localizedDescription]);
    }
    
    [self block]([self arrayFileDownloadData]);
    
    if ([self downloadMode]  == dispatch_semaphore)
        dispatch_semaphore_signal(_semaphore);
    else if ([self downloadMode]  == dispatch_semaphore)
        dispatch_group_leave(_group);
}

- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didWriteData:(int64_t)bytesWritten totalBytesWritten:(int64_t)totalBytesWritten totalBytesExpectedToWrite:(int64_t)totalBytesExpectedToWrite
{
    if (totalBytesExpectedToWrite == NSURLSessionTransferSizeUnknown)
    {
        NSLog(@"Unknown transfer size");
    }
    else
    {
        int index = [self getFileDownloadInfoIndexWithTaskIdentifier:downloadTask.taskIdentifier];
        Item *item = [[self arrayFileDownloadData] objectAtIndex:index];
        item.downloadProgress = (double)totalBytesWritten / (double)totalBytesExpectedToWrite;
    }

    [self block]([self arrayFileDownloadData]);
}

#pragma USNotificationCenterDelegate
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(nonnull UNNotification *)notification withCompletionHandler:(nonnull void (^)(UNNotificationPresentationOptions))completionHandler
{
    completionHandler(UNNotificationPresentationOptionAlert);
}

#pragma self methods
- (void)initializeFileDownloadDataArray
{
    [self setArrayFileDownloadData:[[[NSMutableArray alloc] init] autorelease]];
    [[self arrayFileDownloadData] addObject:[[[Item alloc] initWithFileTitle:@"iOS Programming Guide"
                                                           andDownloadSource:@"http://manuals.info.apple.com/MANUALS/1000/MA1565/en_US/iphone_user_guide.pdf"] autorelease]];
    [[self arrayFileDownloadData] addObject:[[[Item alloc] initWithFileTitle:@"Human Interface Guidelines"
                                                           andDownloadSource:@"http://manuals.info.apple.com/MANUALS/1000/MA1565/en_US/iphone_user_guide.pdf"] autorelease]];
    [[self arrayFileDownloadData] addObject:[[[Item alloc] initWithFileTitle:@"Networking Overview"
                                                           andDownloadSource:@"http://manuals.info.apple.com/MANUALS/1000/MA1565/en_US/iphone_user_guide.pdf"] autorelease]];
    [[self arrayFileDownloadData] addObject:[[[Item alloc] initWithFileTitle:@"AV Foundation"
                                                           andDownloadSource:@"http://manuals.info.apple.com/MANUALS/1000/MA1565/en_US/iphone_user_guide.pdf"] autorelease]];
    [[self arrayFileDownloadData] addObject:[[[Item alloc] initWithFileTitle:@"iPhone User Guide"
                                                           andDownloadSource:@"http://manuals.info.apple.com/MANUALS/1000/MA1565/en_US/iphone_user_guide.pdf"] autorelease]];
    [self block]([self arrayFileDownloadData]);
}

- (void)notifyForDownload:(Item *)item
{
    UNMutableNotificationContent *content = [UNMutableNotificationContent new];
    content.title = @"Item download!";
    content.body = [NSString stringWithFormat:@"Item '%@' is complete downloading", [item title]];
    content.sound = [UNNotificationSound defaultSound];
    UNNotificationTrigger *trigger = [[UNNotificationTrigger alloc] autorelease];
    NSString *identifier = [NSString stringWithFormat:@"%lu", [item identifier]];
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    
    UNNotificationAction *snoozeAction = [UNNotificationAction actionWithIdentifier:@"Snooze"
                                                                              title:@"Okay, later" options:UNNotificationActionOptionNone];
    UNNotificationAction *deleteAction = [UNNotificationAction actionWithIdentifier:@"Delete"
                                                                              title:@"Delete" options:UNNotificationActionOptionDestructive];
    UNNotificationCategory *category = [UNNotificationCategory categoryWithIdentifier:@"UYLReminderCategory"
                                                                              actions:@[snoozeAction, deleteAction] intentIdentifiers:@[]
                                                                              options:UNNotificationCategoryOptionNone];
    NSSet *categories = [NSSet setWithObject:category];
    [center setNotificationCategories:categories];
    content.categoryIdentifier = @"UYLReminderCategory";
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier
                                                                          content:content trigger:trigger];
    [center addNotificationRequest:request withCompletionHandler:^(NSError *_Nullable error)
    {
        if (error != nil)
        {
            NSLog(@"Something went wrong: %@", error);
        }
    }];
    [content release];
}

- (int)getFileDownloadInfoIndexWithTaskIdentifier:(unsigned long)taskIdentifier
{
    int index = 0;
    
    for (int i = 0; i < [[self arrayFileDownloadData] count]; i++)
    {
        Item *item = [[self arrayFileDownloadData] objectAtIndex:i];
        
        if ([item identifier] == taskIdentifier)
        {
            index = i;
        }
    }
    
    return index;
}

- (void)downloadItemsForSemaphoreMode
{
    _semaphore = dispatch_semaphore_create(0);
    dispatch_async(_aQueue, ^
    {
        for (int i = 0; i < [[self arrayFileDownloadData] count]; i++)
        {
            Item *item = [[self arrayFileDownloadData] objectAtIndex:i];
            
            if (!item.isDownload)
            {
                if (item.identifier == -1)
                {
                    item.downloadTask = [[[self.session downloadTaskWithURL:[NSURL URLWithString:item.source]] autorelease] retain];
                }
                
                item.identifier = item.downloadTask.taskIdentifier;
                [item.downloadTask resume];
                item.isDownload = YES;
            }
            
            dispatch_semaphore_wait(_semaphore, DISPATCH_TIME_FOREVER);
        }
        
        [self block]([self arrayFileDownloadData]);
    });
}

- (void)downloadItemsForGroupMode
{
    _aQueue = dispatch_queue_create("newQueue", DISPATCH_QUEUE_CONCURRENT);
    _group = dispatch_group_create();

    for (int i = 0; i < [[self arrayFileDownloadData] count]; i++)
    {
        dispatch_group_enter(_group);
        Item *item = [[self arrayFileDownloadData] objectAtIndex:i];
                           
        if (!item.isDownload)
        {
            if (item.identifier == -1)
            {
                item.downloadTask = [[[self.session downloadTaskWithURL:[NSURL URLWithString:item.source]] autorelease] retain];
            }
                               
            item.identifier = item.downloadTask.taskIdentifier;
            [item.downloadTask resume];
            item.isDownload = YES;
        }
    }
                       
    [self block]([self arrayFileDownloadData]);
}

- (void)downloadItemsForMainMode
{
    for (int i = 0; i < [[self arrayFileDownloadData] count]; i++)
    {
        Item *item = [[self arrayFileDownloadData] objectAtIndex:i];
        dispatch_async(dispatch_get_main_queue(), ^()
        {
            if (!item.isDownload)
            {
                if (item.identifier == -1)
                {
                    item.downloadTask = [[[self.session downloadTaskWithURL:[NSURL URLWithString:item.source]] autorelease] retain];
                }
                
                item.identifier = item.downloadTask.taskIdentifier;
                [item.downloadTask resume];
                item.isDownload = YES;
            }
        
            [self block]([self arrayFileDownloadData]);
        });
    }
}

- (void)downloadItemsForMainModeSync
{
    BOOL __block didRunBlock = NO;
    void (^complition)(void) = ^(void)
    {
        for (int i = 0; i < [[self arrayFileDownloadData] count]; i++)
        {
            Item *item = [[self arrayFileDownloadData] objectAtIndex:i];
            
            if (!item.isDownload)
            {
                if (item.identifier == -1)
                {
                    item.downloadTask = [[[self.session downloadTaskWithURL:[NSURL URLWithString:item.source]] autorelease] retain];
                }
                
                item.identifier = item.downloadTask.taskIdentifier;
                [item.downloadTask resume];
                item.isDownload = YES;
            }
        }
        
        [self block]([self arrayFileDownloadData]);

        didRunBlock = YES;
    };
    
    if ([NSThread isMainThread])
        complition();
    else
        dispatch_sync(dispatch_get_main_queue(), complition);
}

- (void)downloadItemsForGlobalQueue
{
    for (int i = 0; i < [[self arrayFileDownloadData] count]; i++)
    {
        Item *item = [[self arrayFileDownloadData] objectAtIndex:i];
        dispatch_async(_aQueue, ^(void)
        {
            if (!item.isDownload)
            {
                if (item.identifier == -1)
                {
                    item.downloadTask = [[[self.session downloadTaskWithURL:[NSURL URLWithString:item.source]] autorelease] retain];
                }
                
                item.identifier = item.downloadTask.taskIdentifier;
                [item.downloadTask resume];
                item.isDownload = YES;
            }
            
            [self block]([self arrayFileDownloadData]);
        });
    }
}

- (void)downloadItemsForDiffentPrimaryMode
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^(void)
    {
        Item *item0 = [[self arrayFileDownloadData] objectAtIndex:0];
        
        if (!item0.isDownload)
        {
            if (item0.identifier == -1)
            {
                item0.downloadTask = [[[self.session downloadTaskWithURL:[NSURL URLWithString:item0.source]] autorelease] retain];
            }
                           
            item0.identifier = item0.downloadTask.taskIdentifier;
            [item0.downloadTask resume];
            item0.isDownload = YES;
        }
        
        [self block]([self arrayFileDownloadData]);
    });
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void)
    {
        Item *item1 = [[self arrayFileDownloadData] objectAtIndex:1];
        
        if (!item1.isDownload)
        {
            if (item1.identifier == -1)
            {
                item1.downloadTask = [[[self.session downloadTaskWithURL:[NSURL URLWithString:item1.source]] autorelease] retain];
            }
            
            item1.identifier = item1.downloadTask.taskIdentifier;
            [item1.downloadTask resume];
            item1.isDownload = YES;
        }
        
        [self block]([self arrayFileDownloadData]);
    });
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^(void)
    {
        Item *item2 = [[self arrayFileDownloadData] objectAtIndex:2];
        
        if (!item2.isDownload)
        {
            if (item2.identifier == -1)
            {
                item2.downloadTask = [[[self.session downloadTaskWithURL:[NSURL URLWithString:item2.source]] autorelease] retain];
            }
                           
            item2.identifier = item2.downloadTask.taskIdentifier;
            [item2.downloadTask resume];
            item2.isDownload = YES;
        }
        
        [self block]([self arrayFileDownloadData]);
    });
}

- (void)cancelAllDownloads
{
    for (int i = 0; i < [[self arrayFileDownloadData] count]; i++)
    {
        Item *item = [[self arrayFileDownloadData] objectAtIndex:i];
        [[item downloadTask] cancel];
    }
}

@end
