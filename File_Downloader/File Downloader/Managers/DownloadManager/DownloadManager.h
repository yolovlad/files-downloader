//
//  DownloadManager.h
//  File Downloader
//
//  Created by Vlad Yalovenko on 14/11/2016.
//  Copyright © 2016 Vlad Yalovenko. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "DownloadManagerProtocol.h"
#import "Item.h"
#import <UserNotifications/UserNotifications.h>
@interface DownloadManager : NSObject<DownloadManagerProtocol, NSURLSessionDelegate, NSURLSessionDataDelegate, UNUserNotificationCenterDelegate>

@end
