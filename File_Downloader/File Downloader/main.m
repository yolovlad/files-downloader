//
//  main.m
//  File Downloader
//
//  Created by Vlad Yalovenko on 10/11/2016.
//  Copyright © 2016 Vlad Yalovenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
