typedef enum ModeTypes
{ dispatch_semaphore, dispatch_group, dispatch_main_queue, dispatch_main_queue_sync, dispatch_global_queue, dispatch_different_primary_queue
}
DownloadMode;

