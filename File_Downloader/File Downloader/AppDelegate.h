//  ItemCell.m
//  File Downloader
//
//  Created by Vlad Yalovenko on 11/11/2016.
//  Copyright © 2016 Vlad Yalovenko. All rights reserved.
//
#import <UserNotifications/UserNotifications.h>
#import <UIKit/UIKit.h>
@interface AppDelegate : UIResponder<UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property(strong, nonatomic) UIWindow *window;

@end

