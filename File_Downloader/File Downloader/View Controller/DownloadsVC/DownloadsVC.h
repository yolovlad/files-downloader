//
//  DownloadsVC.h
//  File Downloader
//
//  Created by Vlad Yalovenko on 11/11/2016.
//  Copyright © 2016 Vlad Yalovenko. All rights reserved.
//
#import "LoadModeEnum.h"
#include "DownloadManager.h"
#import "DownloadManagerProtocol.h"
#import "ItemCell.h"
#import <UIKit/UIKit.h>

@interface DownloadsVC : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property(nonatomic) DownloadMode downloadMode;
@end
