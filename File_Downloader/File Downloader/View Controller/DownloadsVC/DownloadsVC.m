//
//  DownloadsVC.m
//  File Downloader
//
//  Created by Vlad Yalovenko on 11/11/2016.
//  Copyright © 2016 Vlad Yalovenko. All rights reserved.
//

#import "DownloadsVC.h"

@interface DownloadsVC ()
@property(retain, nonatomic) IBOutlet UITableView *tableView;
@property(retain, nonatomic) NSMutableArray *itemList;
@property(retain, nonatomic) DownloadManager *downloadManagerAnchor;
@property(assign, nonatomic) id<DownloadManagerProtocol> downloadManager;
@end

@implementation DownloadsVC
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self tableView] registerNib:[UINib nibWithNibName:@"ItemCell" bundle:nil]  forCellReuseIdentifier:@"ItemCell"];
    [self setDownloadManagerAnchor:[[[DownloadManager alloc] init] autorelease]];
    [self setDownloadManager:[self downloadManagerAnchor]];
    [[self downloadManager] downloadItemsForMode:[self downloadMode] WithComplition:^(NSArray *itemData)
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            [self setItemList:[[itemData mutableCopy] autorelease]];
            [[self tableView] reloadData];
        });
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [_itemList release];
    [_tableView release];
    [_downloadManagerAnchor release];
    [super dealloc];
}

#pragma tableView DataSource/Delegate
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ItemCell" forIndexPath:indexPath];
    [cell setupWithItem:[[self itemList] objectAtIndex:[indexPath row]]];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self itemList] count];
}

#pragma self methods
- (IBAction)didClickDoneButton:(id)sender
{
    [[self downloadManager] cancelAllDownloads];
    [self dismissViewControllerAnimated:self completion:nil];
}

@end
