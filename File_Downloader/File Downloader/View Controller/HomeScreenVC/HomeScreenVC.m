#import "HomeScreenVC.h"

@interface HomeScreenVC ()
@property(nonatomic) DownloadMode downloadMode;
@property (retain, nonatomic) IBOutlet UILabel *titleName;

@end

@implementation HomeScreenVC

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [_titleName release];
    [super dealloc];
}

#pragma self methods
- (IBAction)clickUploadedButton:(id)sender
{
    [self presentViewController:[self createPickModeAlert] animated:YES completion:nil];
}

- (void)setUpDownloadMode:(DownloadMode)mode
{
    [self setDownloadMode:mode];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"filesVCSegue"])
    {
        DownloadsVC *downloadVC = [segue destinationViewController];
        [downloadVC setDownloadMode:[self downloadMode]];
    }
}

- (UIViewController *)createPickModeAlert
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:
                                @"Choose download type" message:@"Select the appropriate option" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *actionSemaphore = [UIAlertAction actionWithTitle:@"dispatch_semaphore"
                                                                     style:UIAlertActionStyleDefault
                                                                   handler:^(UIAlertAction *_Nonnull action)
                                                                    {
                                                                        [self setUpDownloadMode:dispatch_semaphore];
                                                                        [self performSegueWithIdentifier:@"filesVCSegue" sender:self];
                                                                    }];
    
    UIAlertAction *actionDispathGroup = [UIAlertAction actionWithTitle:@"dispatch_group"
                                                                                  style:UIAlertActionStyleDefault
                                                                                handler:^(UIAlertAction *_Nonnull action)
                                                                                {
                                                                                    [self setUpDownloadMode:dispatch_group];
                                                                                    [self performSegueWithIdentifier:@"filesVCSegue" sender:self];
                                                                                }];
    
    UIAlertAction *actionMainQueue = [UIAlertAction actionWithTitle:@"dispatch_async Main queue"
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction *_Nonnull action)
                                                                    {
                                                                        [self setUpDownloadMode:dispatch_main_queue];
                                                                        [self performSegueWithIdentifier:@"filesVCSegue" sender:self];
                                                                    }];
    
    UIAlertAction *actionMainQueueSync = [UIAlertAction actionWithTitle:@"dispatch_sync Main queue"
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction *_Nonnull action)
                                                            {
                                                                [self setUpDownloadMode:dispatch_main_queue_sync];
                                                                [self performSegueWithIdentifier:@"filesVCSegue" sender:self];
                                                            }];
    
    UIAlertAction *actionGlobalQueue = [UIAlertAction actionWithTitle:@"dispatch_async Concurrent queue"
                                                              style:UIAlertActionStyleDefault
                                                            handler:^(UIAlertAction *_Nonnull action)
                                                            {
                                                                [self setUpDownloadMode:dispatch_global_queue];
                                                                [self performSegueWithIdentifier:@"filesVCSegue" sender:self];
                                                            }];
    
    UIAlertAction *actionDifferentQueue = [UIAlertAction actionWithTitle:@"Download with differrent queue"
                                                                style:UIAlertActionStyleDefault
                                                              handler:^(UIAlertAction *_Nonnull action)
                                                                {
                                                                    [self setUpDownloadMode:dispatch_different_primary_queue];
                                                                    [self performSegueWithIdentifier:@"filesVCSegue" sender:self];
                                                                }];
    
    UIAlertAction *actionCancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *_Nonnull action)
                                                            {
                                                                NSLog(@"Cancel action selected");
                                                            }];
    [alert addAction:actionSemaphore];
    [alert addAction:actionDispathGroup];
    [alert addAction:actionMainQueue];
    [alert addAction:actionMainQueueSync];
    [alert addAction:actionGlobalQueue];
    [alert addAction:actionDifferentQueue];
    [alert addAction:actionCancel];
    return alert;
}

@end
