//
//  ItemCell.m
//  File Downloader
//
//  Created by Vlad Yalovenko on 11/11/2016.
//  Copyright © 2016 Vlad Yalovenko. All rights reserved.
//

#import "ItemCell.h"
@interface ItemCell ()
@property(retain, nonatomic) IBOutlet UILabel *name;
@property(retain, nonatomic) IBOutlet UIImageView *statusImage;
@property(retain, nonatomic) IBOutlet UILabel *progressPercent;
@property(retain, nonatomic) IBOutlet UIProgressView *progressbar;

@end
@implementation ItemCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)dealloc
{
    [_name release];
    [_progressPercent release];
    [_statusImage release];
    [_progressbar release];
    [super dealloc];
}

#pragma self-methods
- (instancetype)setupWithItem:(Item *)item;
{
    if ([item downloadComplete])
    {
        [[self statusImage] setHidden:NO];
        [[self statusImage] setImage:[UIImage imageNamed:@"Done"]];
    }
    else
    {
        [[self statusImage] setHidden:YES];
    }
    
    [[self name] setText:[item title]];
    [[self progressPercent] setText:[NSString stringWithFormat:@"%.0f%%", [item downloadProgress] * 100]];
    [[self progressbar] setProgress:[item downloadProgress]];
    return self;
}

@end
