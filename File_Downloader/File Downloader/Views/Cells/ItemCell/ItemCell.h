//
//  ItemCell.h
//  File Downloader
//
//  Created by Vlad Yalovenko on 11/11/2016.
//  Copyright © 2016 Vlad Yalovenko. All rights reserved.
//
#import "Item.h"
#import <UIKit/UIKit.h>

@interface ItemCell : UITableViewCell
- (instancetype)setupWithItem:(Item *)item;

@end
