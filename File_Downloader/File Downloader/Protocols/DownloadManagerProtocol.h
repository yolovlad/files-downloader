#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "LoadModeEnum.h"
typedef void (^complitionBlock)(NSArray *);

@protocol DownloadManagerProtocol<NSObject>

@required
- (NSArray *)getDownloadingItems;
- (void)downloadItemsForMode:(DownloadMode)mode WithComplition:(complitionBlock)updateBlock;
- (void)cancelAllDownloads;
@end
