//
//  Item.h
//  File Downloader
//
//  Created by Vlad Yalovenko on 11/11/2016.
//  Copyright © 2016 Vlad Yalovenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Item : NSObject
@property(nonatomic, retain) NSString *title;
@property(nonatomic, retain) NSString *source;
@property(nonatomic, retain) NSURLSessionDownloadTask *downloadTask;
@property(nonatomic) unsigned long identifier;
@property(nonatomic) double downloadProgress;
@property(nonatomic) bool isDownload;
@property(nonatomic) bool downloadComplete;

- (id)initWithFileTitle:(NSString *)title andDownloadSource:(NSString *)source;
@end
