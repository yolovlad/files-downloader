//
//  Item.m
//  File Downloader
//
//  Created by Vlad Yalovenko on 11/11/2016.
//  Copyright © 2016 Vlad Yalovenko. All rights reserved.
//

#import "Item.h"
@implementation Item

- (id)initWithFileTitle:(NSString *)title andDownloadSource:(NSString *)source
{
    self = [super init];
    
    if (self == [super init])
    {
        [self setTitle:title];
        [self setSource:source];
        self.downloadProgress = 0.0;
        [self setIsDownload:NO];
        [self setIdentifier:-1];
        [self setDownloadComplete:NO];
    }
    
    return self;
}

- (void)dealloc
{
    [_downloadTask release];
    [_source release];
    [_title release];
    [super dealloc];
}

@end
